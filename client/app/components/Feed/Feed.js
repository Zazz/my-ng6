import angular from 'angular';
import FeedComponent from './Feed.component';
import FeedController from './Feed.controller';

let FeedModule = angular.module('Feed', [
    'ui.router'
])

.config(($stateProvider, $urlRouterProvider) => {
//  $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('feed', {
        url: '/feed',
        template: '<feed></feed>'
    });
})
.controller('FeedController', FeedController)
.directive('feed', FeedComponent);

export default FeedModule;

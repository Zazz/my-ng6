class <%= capitalizeName %>Model {
    constructor(id)
    {
        this.id = id;
    }
}

export default <%= capitalizeName %>Model;

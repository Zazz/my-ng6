let Environment = function(){
    this.os = {
        isWindows:  process.platform == "win32",
        isMac:      process.platform == "darwin",
        isLinux:    process.platform == "linux",
        isFreeBSD:  process.platform == "freebsd",
        isSun:      process.platform == "sunos"
    };
    this.getOS = function(){
        if (this.os.isWindows) return "Windows";
        if (this.os.isMac)     return "Mac";
        if (this.os.isLinux)   return "Linux";
        if (this.os.isFreeBSD) return "FreeBSD";
        if (this.os.isSun)     return "SunOS";
        return "unknown";
    }
};
Environment.prototype.toString = function() { return "[OS: "+this.getOS()+"]" };

export default Environment;
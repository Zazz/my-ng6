import angular  from 'angular';

import <%= capitalizeName %>Component from './<%= name %>.component';
import <%= capitalizeName %>Controller from './<%= name %>.controller';

// Create <%= startCaseName %> module
let <%= capitalizeName %>Module = angular.module('<%= name %>', [
//  'ui.router'
])

/*
// Define route for <%= startCaseName %>
.config(($stateProvider, $urlRouterProvider) => {
    //$urlRouterProvider.otherwise('/');

    $stateProvider.state('<%= lowerName %>', {
        url: '/<%= lowerName %>',
        template: '<<%= kebabCaseName %>></<%= kebabCaseName %>>'
    });
})
*/

.controller('<%= capitalizeName %>Controller', <%= capitalizeName %>Controller)
.directive('<%= camelCaseName %>', <%= capitalizeName %>Component); // <<%= kebabCaseName %>></<%= kebabCaseName %>>

export default <%= capitalizeName %>Module;

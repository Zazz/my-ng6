let $q = angular.injector(['ng']).get('$q');
let stub = {};

stub.index = function(){
    let deferred = $q.defer();
    deferred.resolve({
        data: [
            {id: 1, userId: 1, title: "One",   body: "First" },
            {id: 2, userId: 2, title: "Two",   body: "Second"},
            {id: 3, userId: 3, title: "Three", body: "Third" }
        ],
        status: 200
    });
    return deferred.promise;
};

stub.show = function(id){
    let deferred = $q.defer();
    deferred.resolve({
        data: {id: id, userId: id, title: "Post #"+id, body: "Post Body"},
        status: 200
    });
    return deferred.promise;
};

stub.store = function(data) {
    let deferred = $q.defer();
    deferred.resolve({
        data: {id: 100, userId: 100, title: "New Post", body: "Post Body"},
        status: 200
    });
    return deferred.promise;
};

stub.replace = function(id, data) {
    data.id = id;

    let deferred = $q.defer();
    deferred.resolve({
        data: data,
        status: 200
    });
    return deferred.promise;
};

stub.update = function(id, data) {
    let updated = {userId: 100, title: "Post #"+id, body: "Post Body"};
    for (let key in data) updated[key] = data[key];
    updated.id = id;

    let deferred = $q.defer();
    deferred.resolve({
        data: updated,
        status: 200
    });
    return deferred.promise;
};

export default stub;
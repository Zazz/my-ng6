module.exports = {
    devtool: 'sourcemap',
    output: {
        filename: 'bundle.js'
    },
    externals: {
        'angular': "angular"
    },

    module: {
        loaders: [
            { test: /\.js$/,    loader: 'babel', exclude: [/app\/lib/, /node_modules/] },
            { test: /\.html$/,  loader: 'raw' },
            { test: /\.styl$/,  loader: 'style!css!stylus' },
            { test: /\.scss$/,  loaders: ['style', 'css', 'sass'] },
            { test: /\.css$/,   loaders: ['style', 'css'] }
        ]
    }
};

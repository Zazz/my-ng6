import <%= capitalizeName %>Module     from './<%= name %>'
import <%= capitalizeName %>Controller from './<%= name %>.controller';
import <%= capitalizeName %>Component  from './<%= name %>.component';
import <%= capitalizeName %>Template   from './<%= name %>.html';

describe('<%= startCaseName %>', () => {

    //////////////////////////////////////////////////////////////////////////////
    //                                  SETUP                                   //
    //////////////////////////////////////////////////////////////////////////////

    let makeController, $rootScope, $scope;

    beforeEach(window.module(<%= capitalizeName %>Module.name));

    // Fetch dependencies
    beforeEach(inject((
        _$rootScope_
    ) => {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
    }));

    // Inject dependencies or their mocks into controller instantiated by makeController()
    beforeEach(inject(($controller) => {
        makeController = function(){
            // Injection locals for Controller.
            let locals = {
                $rootScope: $rootScope,
                $scope: $scope
            };
            // Properties to add to the controller before invoking the constructor.
            // This is used to simulate the bindToController feature and simplify certain kinds of tests.
            let bindings = {

            };
            // Create an instance of the controller
            return $controller('<%= capitalizeName %>Controller', locals, bindings);
        };
    }));

    //////////////////////////////////////////////////////////////////////////////
    //                                  MODULE                                  //
    //////////////////////////////////////////////////////////////////////////////

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                CONTROLLER                                //
    //////////////////////////////////////////////////////////////////////////////

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                 TEMPLATE                                 //
    //////////////////////////////////////////////////////////////////////////////

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(<%= capitalizeName %>Template).to.match(/{{\s?vm\.name\s?}}/g);
        });
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                COMPONENT                                 //
    //////////////////////////////////////////////////////////////////////////////

    describe('Component', () => {
        // component/directive specs
        let component = <%= capitalizeName %>Component();

        it('includes the intended template',() => {
            expect(component.template).to.equal(<%= capitalizeName %>Template);
        });

        it('uses `controllerAs` syntax', () => {
            expect(component).to.have.property('controllerAs');
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(<%= capitalizeName %>Controller);
        });
    });

    //////////////////////////////////////////////////////////////////////////////
});

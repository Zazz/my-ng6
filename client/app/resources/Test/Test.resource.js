class TestResource {
    constructor($http, $q) {
        this.$http = $http;
        this.$q = $q;
        this.baseUrl = "http://jsonplaceholder.typicode.com/posts/";
    } // End of constructor

    /**
     * GET a list of resource items
     * @return {Promise}
     */
    index() {
        return this.$http.get(
            this.baseUrl
        );
    }

    /**
     * GET a specific resource with a given ID
     * @param {number|string} id The ID of the resource
     * @return {Promise}
     * @throws Exception
     */
    show(id) {
        if (id <= 0) throw "Invalid ID";

        return this.$http.get(
            this.baseUrl+id
        );
    }

    /**
     * POST a new resource item
     * @param {object} data A complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    store(data) {
        if (id <= 0) throw "Invalid ID";
        if (!data) throw "No data";

        return this.$http.post(
            this.baseUrl,
            data
        );
    }

    /**
     * PUT a new resource item at a given ID
     * @param {number|string} id The ID of the resource
     * @param {object} data A complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    replace(id, data) {
        if (id <= 0) throw "Invalid ID";
        if (!data) throw "No data";

        return this.$http.put(
            this.baseUrl+id,
            data
        );
    }

    /**
     * PATCH an existing resource with the given ID
     * @param {number|string} id The ID of the resource
     * @param {object} data A partial/complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    update(id, data) {
        if (id <= 0) throw "Invalid ID";
        if (!data) throw "No data";

        return this.$http.patch(
            this.baseUrl+id,
            data
        );
    }
}

export default TestResource;
/**
 * @class Description of <%= startCaseName %>
 */
class <%= capitalizeName %>Resource {
    constructor($http, $q) {
        this.$http = $http;
        this.$q = $q;
        this.baseUrl = APP_URL + "/api/<%= kebabCaseName %>";
    }

    /**
     * GET a list of resource items
     * @return {Promise}
     */
    index() {
        let resource = this;
        let deferred = resource.$q.defer();

        //Fetch
        let promise = resource.$http.get( resource.indexURL() );

        // Intercept response
        promise.then(
            (success) => { deferred.resolve(success); },
            (failed) =>  { deferred.reject(failed);   }
        );

        return deferred.promise;
    }
    indexURL() {
        return this.baseUrl;
    }

    /**
     * GET a specific resource with a given ID
     * @param {number|string} ref The ID/slug of the resource
     * @return {Promise}
     * @throws Exception
     */
    show(ref) {
        let resource = this;
        let deferred = resource.$q.defer();

        //Fetch
        let promise = resource.$http.get( resource.showURL(ref) );

        // Intercept response
        promise.then(
            (success) => { deferred.resolve(success); },
            (failed) =>  { deferred.reject(failed);   }
        );

        return deferred.promise;
    }
    showURL(ref) {
        if (!ref) throw "Invalid reference";
        let id = parseInt(ref);
        if (!isNaN(id) && id <= 0) throw "Invalid ID";

        return this.baseUrl+'/'+ref;
    }

    /**
     * POST a new resource item
     * @param {object} data A complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    store(data) {
        if (!data) throw "No data";

        let promise = this.$http.post( this.storeURL(), data );

        // Intercept response
        promise.then(
            (success) => { deferred.resolve(success); },
            (failed) =>  { deferred.reject(failed);   }
        );

        return deferred.promise;
    }
    storeURL() {
        return this.baseUrl;
    }

    /**
     * PUT a new resource item at a given ID
     * @param {number|string} ref The ID of the resource
     * @param {object} data A complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    replace(ref, data) {
        if (!data) throw "No data";

        let promise = this.$http.put( this.replaceURL(ref), data );

        // Intercept response
        promise.then(
            (success) => { deferred.resolve(success); },
            (failed) =>  { deferred.reject(failed);   }
        );

        return deferred.promise;
    }
    replaceURL(ref) {
        if (!ref) throw "Invalid reference";
        let id = parseInt(ref);
        if (!isNaN(id) && id <= 0) throw "Invalid ID";

        return this.baseUrl+'/'+ref;
    }

    /**
     * PATCH an existing resource with the given ID
     * @param {number|string} ref The ID of the resource
     * @param {object} data A partial/complete set of resource data
     * @return {Promise}
     * @throws Exception
     */
    update(ref, data) {
        if (!data) throw "No data";

        let promise = this.$http.patch( this.updateURL(ref), data );

        // Intercept response
        promise.then(
            (success) => { deferred.resolve(success); },
            (failed)  => { deferred.reject(failed);   }
        );

        return deferred.promise;
    }
    updateURL(ref) {
        if (!ref) throw "Invalid reference";
        let id = parseInt(ref);
        if (!isNaN(id) && id <= 0) throw "Invalid ID";

        return this.baseUrl+'/'+ref;
    }
}

export default <%= capitalizeName %>Resource;

import template from './Feed.html';
import controller from './Feed.controller';
import './Feed.scss';

let FeedComponent = function () {
  return {
    restrict: 'E',
    scope: {},
    template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  };
};

export default FeedComponent;

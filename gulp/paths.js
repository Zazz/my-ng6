import path from 'path';

const projectRoot = '.';
const projectRootAbsolute = path.join(__dirname, '..');
const sourceRoot = 'client';
const targetRoot = 'dist';

// helper method for resolving paths
let resolveToApp = function(glob){
    glob = glob || '';
    return path.join(sourceRoot, 'app', glob); // app/{glob}
};

// map of all paths
let paths = {
    baseUrl: {
        local: "localhost"
    },
    source: {
        app: {
            root:       resolveToApp(),
            entry:      resolveToApp('app.js'),
            components: resolveToApp('components'),
            resources:  resolveToApp('resources'),
            directives: resolveToApp('directives'),
            models:     resolveToApp('models')
        },
        scripts: path.join(sourceRoot, 'scripts'),
        styles:  path.join(sourceRoot, 'styles'),
        vendor:  path.join(sourceRoot, 'vendor'),
        root: sourceRoot
    },
    target: {
        scripts: path.join(targetRoot, 'scripts'),
        styles:  path.join(targetRoot, 'styles'),
        vendor:  path.join(targetRoot, 'vendor'),
        root: targetRoot
    },
    project: {
        root: projectRoot,
        rootAbsolute: projectRootAbsolute,
        generators: {
            component: path.join(projectRoot, 'generator/component/**/*.**'),
            resource:  path.join(projectRoot, 'generator/resource/**/*.**'),
            directive: path.join(projectRoot, 'generator/directive/**/*.**'),
            model:     path.join(projectRoot, 'generator/model/**/*.**')
        },
        bower:       path.join(projectRoot, 'bower_components'),
        node:        path.join(projectRoot, 'node_modules'),
        temp:        path.join(projectRoot, '.tmp'),
        tests:       path.join(projectRoot, 'tests'),
        testResults: path.join(projectRoot, 'tests/results'),
        config:      path.join(projectRoot, 'config')
    }
};

export default paths;
import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Feed from './Feed/Feed';

let componentModule = angular.module('app.components', [
  Home.name,
  About.name,
  Feed.name
]);

export default componentModule;

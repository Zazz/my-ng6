import gulp     from 'gulp';
import gutil    from 'gulp-util';
import path     from 'path';
import paths    from './paths';
import lodash   from 'lodash';
import prompt   from 'gulp-prompt';
import template from 'gulp-template';
import rename   from 'gulp-rename';
import notifier from 'node-notifier';   // For notifications outside of a stream

/**
 * Create an object containing name templates for the 'make' tasks.
 * @param {string} name
 * @param {string} parentPath
 * @param {string} destPath
 * @returns {object}
 */
const createNameTemplates = function (name, parentPath="", destPath="") {
    let parentPathForward = parentPath.replace(/\\/g, '/');

    return {
        name:            name,
        upperName:       name.toUpperCase(),
        lowerName:       name.toLowerCase(),
        capitalizeName:  lodash.capitalize(name),    // fooBar  => FooBar
        camelCaseName:   lodash.camelCase(name),     // foo-bar => fooBar
        kebabCaseName:   lodash.kebabCase(name),     // fooBar  => foo-bar
        snakeCaseName:   lodash.snakeCase(name),     // fooBar  => foo_bar
        startCaseName:   lodash.startCase(name),     // fooBar  => Foo Bar
        parentPath:      parentPathForward,
        slashParentPath: parentPathForward ? '/' + parentPathForward : '',
        dotParentPath:   parentPathForward ? '.' + parentPathForward.replace(new RegExp("/", 'g'), '\.') : '',
        destPath:        destPath.replace(/\\/g, '/')

    };
};

/**
 * Creates an element with the specified generator.
 * @param {string} generatorPath   The directory where the element's generator lives
 * @param {string} destinationPath The directory where the element should be sent to
 * @param {function} callback      Callback
 * @returns Stream
 */
const makeFromTemplate = function(generatorPath, destinationPath, callback) {
    if (typeof generatorPath != 'string' || !generatorPath) throw "generatorPath was not passed";
    if (typeof destinationPath != 'string' || !destinationPath) throw "destinationPath was not passed";
    if (typeof callback != 'function') throw "callback was not passed";

    // Get the template files
    let stream = gulp.src(
        generatorPath
    ).pipe(prompt.prompt([{
        message: 'Name (Required)',
        name: 'name',
        type: 'input'
    },{
        message: 'Parent Path (optional)',
        name: 'parent',
        type: 'input'
    }], function(res)
    {
        // Get passed params
        let name = res.name;
        let parentPath = res.parent || '';

        // Process input
        if (!name) throw "Name is required";
        name = name.replace(/ /g,'');
        parentPath = path.join(parentPath.replace(/ /g,'').toLowerCase());
        destinationPath = path.join(destinationPath, parentPath, name.toLowerCase());

        // Continue stream with input
        stream.pipe(
            // Replace instances of <%= key %> in the files with 'value' from this template object
            template(createNameTemplates(name, parentPath, destinationPath))
        ).pipe(
            // Rename the files by replacing 'temp' with 'name'
            rename((path) => {
                path.basename = path.basename.replace('temp', name);
            })
        ).pipe(
            // Copy to destinationPath
            gulp.dest(destinationPath)
        ).on('end', function(){
            // Show notification
            notifier.notify({
                title: "Generator",
                message: destinationPath,
                icon: path.join(paths.project.node, 'gulp-notify/assets/gulp.png')
            });
            gutil.log(name, "created in", destinationPath);

            // Allow time for notification, then force exit because gulp-prompt doesn't seem to terminate
            setTimeout(process.exit, 500);
            callback(); // Attempt to exit normally
        });
    }));
};

export {makeFromTemplate};
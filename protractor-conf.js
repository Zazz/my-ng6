exports.config = {
    // If you're running 'node_modules\.bin\webdriver-manager.cmd start' manually
    //seleniumAddress: 'http://localhost:4444/wd/hub',

    // Start selenium separately
    seleniumServerJar: 'node_modules/protractor/selenium/selenium-server-standalone-2.48.2.jar',
    chromeDriver: 'node_modules/protractor/selenium/chromedriver.exe',

    framework: 'jasmine',
    specs: ['protractor-spec.js']
};
class FeedController {
    constructor($rootScope, $scope, TestResource) {
        let ci = this;
        this.name = 'Feed';

        console.log("FeedController", "TestResource", TestResource);

        TestResource.index().then(
            (success) => {
                console.log("TestResource.list Success", success);
                ci.request_index = { type: 'success', data: success };
            },
            (error) => {
                console.log("TestResource.list error", error);
                ci.request_index = { type: 'error', data: error};
            }
        );
        TestResource.show(3).then(
            (success) => {
                console.log("TestResource.find Success", success);
                ci.request_show = { type: 'success', data: success };
            },
            (error) => {
                console.log("TestResource.find error", error);
                ci.request_show = { type: 'error', data: error};
            }
        );
    }
}

export default FeedController;

import <%= capitalizeName %>MockData from './world.mockdata';

/**
 * An object containing stub methods.
 * @example sinon.stub(my<%= capitalizeName %>ResourceInstance, 'index', <%= capitalizeName %>Stub.index);
 */
let stub = {};
let $q = angular.injector(['ng']).get('$q');

stub.index = function(){
    let deferred = $q.defer();
    deferred.resolve({
        data: <%= capitalizeName %>MockData.index(),
        status: 200
    });
    return deferred.promise;
};

stub.show = function(id){
    let deferred = $q.defer();
    deferred.resolve({
        data: <%= capitalizeName %>MockData.show(),
        status: 200
    });
    return deferred.promise;
};

export default stub;

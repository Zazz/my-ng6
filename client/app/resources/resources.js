import angular  from 'angular';

import Test from './Test/Test';

let resourceModule = angular.module('app.resources', [
    Test.name
]);

export default resourceModule;

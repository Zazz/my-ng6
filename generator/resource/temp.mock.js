/**
 * A Sinon mock of <%= name %> to be used in tests
 */
export let <%= name %>Mock = {
    create: function() {
        return sinon.stub({

            /**
             * Mock of the <%= name %> 'list' request
             * @returns JSON
             */
            list: function () {
                return {};
            },

            /**
             * Mock of the <%= name %> 'find' request
             * @param id
             * @returns {{}}
             */
            find: function (id) {
                return {};
            }

        });
    }
};
import {protractor, webdriver_update} from 'gulp-protractor';
import path     from 'path';
import paths    from './paths';
import notifier from 'node-notifier';   // For notifications outside of a stream
import debug    from 'gulp-debug';

/**
 * Get protractor configuration options that will automatically start a selenium instance
 * @param {function} callback
 * @param {int} [retries=0] +1 when calling again after an auto-update
 * @private
 */
const _getProtractorConfig = function(callback, retries=0)
{
    // Initial Protractor options
    let options = {
        args: ['--baseUrl', paths.baseUrl.local],
        configFile: path.join(paths.project.config, 'protractor.config.js')
    };
    let driverName = 'chromeDriver';

    // Selenium may be in either of these two paths
    let seleniumPaths = [
        path.join(paths.project.node, "protractor/selenium"),
        path.join(paths.project.node, "gulp-protractor/node_modules/protractor/selenium")
    ];

    // Find driver(s) and server jar
    let driverExes = [];
    let serverJars = [];
    for (let i=0; i < seleniumPaths.length; i++){
        let seleniumPath = seleniumPaths[i];
        let driverPath = path.join(seleniumPath, "chromedriver?(.exe)"); // 'chromedriver' or 'chromedriver.exe'
        let serverPath = path.join(seleniumPath, "selenium-server-standalone-*.jar");
        driverExes = driverExes.concat(glob.sync(driverPath));
        serverJars = serverJars.concat(glob.sync(serverPath));
    }

    // Process missing
    let missingFiles = [];
    if (!driverExes.length) missingFiles.push(driverName);
    if (!serverJars.length) missingFiles.push("selenium server .jar");

    // Success: Files found
    if (!missingFiles.length){
        options.seleniumServerJar = serverJars[0];  // Add found selenium server jar
        options[driverName] = driverExes[0];        // Add found driver
        callback.call(this, options);               // Pass options to callback
        return;
    }

    // Warning: Files missing. Try again after auto-update
    if (!retries){
        notifier.notify({
            title: "Selenium file warning",
            message: "No "+missingFiles.join(" or ")+" found.\nRunning ebdriver-manager update"
        });
        // Should be same as running 'node_modules/.bin/webdriver-manager update'
        webdriver_update(null, function(){
            _getProtractorConfig(callback, retries ? retries+1 : 1); // Try again after update
        });
        return;
    }

    // Error: Files still missing after auto-update.
    let note = {
        title: "Selenium file error",
        message: "Failed to auto-update "+missingFiles.join(" or ")+".\n Try running 'node_modules\\.bin\\webdriver-manager update"
    };
    notifier.notify(note);
    throw note.message;
};

/**
 * Start a protractor E2E test run
 * @param {string[]} specFiles Globs for the spec files to be run
 * @param {function} [callback]
 */
const startProtractor = function(specFiles, callback)
{
    if (!specFiles || !specFiles.length) throw "No protractor tests passed";

    // Get protractor config
    _getProtractorConfig(function(options){
        gulp.src(
            specFiles
        ).pipe(
            debug({ title: "Protractor scripts"  })
        ).pipe(protractor(
            options
        )).on('error', gulpNotify.onError(function(error){
            return { title: "Protractor Error", message: error.message };
        })).on('end',function(){
            notifier.notify({
                title: "Protractor",
                message: "Finished",
                icon: path.join(paths.project.node, 'gulp-notify/assets/gulp.png')
            });
            if (callback) callback.apply(this);
        })
    });
};

export {startProtractor};
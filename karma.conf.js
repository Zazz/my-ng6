module.exports = function (config) {
    config.set({
        // base path used to resolve all patterns
        basePath: '',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'chai', 'sinon'],

        // list of files/patterns to load in the browser
        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-animate/angular-animate.js',
            'bower_components/angular-aria/angular-aria.js',
            'bower_components/angular-material/angular-material.js',

            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-material/angular-material-mocks.js',
            { pattern: 'spec.bundle.js', watched: false } // App entry point
        ],

        // files to exclude
        exclude: [],

        plugins: [
            require("karma-chai"),
            require("karma-chrome-launcher"),
            require("karma-mocha"),
            require("karma-sinon"),
            require("karma-mocha-reporter"),
            require("karma-sourcemap-loader"),
            require("karma-webpack"),
            require("karma-coverage")
        ],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: { 'spec.bundle.js': ['webpack', 'sourcemap'] },

        webpack: {
            devtool: 'inline-source-map',
            module: {
                preLoaders: [{
                    test:/\.js$/,
                    loader: 'isparta',
                    exclude: [/app\/lib/, /node_modules/, /\.spec\.js$/, /\.stub\.js$/ ]
                }],
                loaders: [
                    { test: /\.js$/,  loader: 'babel', exclude:[/app\/lib/,  /node_modules/, /bower_components/] },
                    { test: /\.html$/,  loader: 'raw' },
                    { test: /\.styl$/,  loader: 'style!css!stylus' },
                    { test: /\.scss$/,  loader: 'style!css!sass' },
                    { test: /\.css$/,   loader: 'style!css' }
                ]
            }
        },

        webpackServer: {
            noInfo: true // prevent console spamming when running in Karma!
        },

        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: [
            'progress',
            'mocha',
            'coverage'
        ],
        coverageReporter: {
            reporters: [
                {type: 'lcovonly',      dir: 'coverage/',       subdir: '.'},
                {type: 'json',          dir: 'coverage/',       subdir: '.'},
                {type: 'html',          dir: 'coverage/',       subdir: 'html'},
                {type: 'text',          dir: 'coverage/log',    file: 'coverage.txt'},
                {type: 'text-summary',  dir: 'coverage/log',    file: 'summary.txt'},
                {type: 'text-summary'}
            ]
        },

        // web server port
        port: 9876,

        // enable colors in the output
        colors: true,

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // toggle whether to watch files and rerun tests upon incurring changes
        //autoWatch: true,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],

        // if true, Karma runs tests once and exits
        //singleRun: false
    });
};

import <%= capitalizeName %>Model from './<%= name %>.model';

let <%= capitalizeName %>Factory = {
    createFromResponse(response){
        return new <%= capitalizeName %>Model(
            response.<%= snakeCaseName %>_id
        );
    }
};

export default <%= capitalizeName %>Factory;

'use strict';

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                              IMPORTS                                               //
////////////////////////////////////////////////////////////////////////////////////////////////////////

import gulp         from 'gulp';
import gutil        from 'gulp-util';
import notifier     from 'node-notifier';   // For notifications outside of a stream
import gulpNotify   from 'gulp-notify';     // For notifications within a stream
import size         from 'gulp-size';
import webpack      from 'webpack-stream';
import path         from 'path';
import sync         from 'run-sequence';
import serve        from 'browser-sync';
import fs           from 'fs';
import del          from 'del';
import eslint       from 'gulp-eslint';
import bower        from 'main-bower-files';
import mkdirp       from 'mkdirp';

let reload = () => serve.reload();

// Task helpers
import Environment                      from './gulp/Environment';
import paths                            from './gulp/paths';
import {startKarma, getCoverageSummary} from './gulp/karma';
import {startProtractor}                from './gulp/protractor';
import {makeFromTemplate}               from './gulp/generators';

const environment = new Environment();
console.log("Detecting environment: "+environment);

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          MISC FUNCTIONS                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the contents of .gitingore in an array
 * @throws Exception May fail to read .gitignore
 * @returns {string[]|boolean}
 */
function getGitIgnore(){
    if (_getGitIgnoreCache !== null) return _getGitIgnoreCache; // Return cached

    let ignoreFile = fs.readFileSync(".gitignore");                                             // Load the file
    let ignoreText = ignoreFile ? ignoreFile.toString() : false;                                // Read the contents
    let ignoreArray = ignoreText ? ignoreText.split('\n') : false;                              // Split by line
    ignoreArray = ignoreArray ? ignoreArray.map(function(str){ return str.trim(); }) : false;   // Trim
    ignoreArray = ignoreArray ? ignoreArray.filter(function(str){ return str.length; }) : [];   // Filter out empty lines

    return _getGitIgnoreCache = ignoreArray; // Cache and return
}
var _getGitIgnoreCache = null;

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                              CLEAN                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Perform all cleanup operations
 */
gulp.task('clean', ['clean:source', 'clean:target', 'clean:temp', 'clean:testResults']);

/**
 Clear out target directories
 */
gulp.task('clean:target', (callback) => {
    let sources = [
        paths.target.root+'/*',
        '!'+paths.target.root+'/.git'
    ];
    return del(sources, {dot: true}, callback)
});

/**
 Refresh source directories
 */
gulp.task('clean:source', (callback) => {
    let sources = [
        paths.source.vendor
    ];
    return del(sources, {dot: true}, callback)
});

gulp.task('clean:temp', (callback) => {
    let sources = [
        paths.project.temp
    ];
    return del(sources, {dot: true}, callback)
});

/**
 Clear out output directory used for storing test-results
 */
gulp.task('clean:testResults', (callback) => {
    let sources = [
        paths.project.testResults+'/**/*',
        '!'+paths.project.testResults+'/.gitignore'
    ];
    return del(sources, {dot: true}, callback)
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            COPY                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('copy', ['copy:root', 'copy:bower']);

/**
 * Copy the root of the source
 */
gulp.task('copy:root', () => {
    let includes = [
        paths.source.root+'/*',
        path.join(paths.project.node, 'apache-server-configs/dist/.htaccess')
    ];
    let excludes = [
        path.join(paths.source.root, 'app')
    ];

    // Exclude .gitignore files by default
    let ignore = getGitIgnore();
    if (ignore) excludes = ignore.concat(excludes);

    // Prepare excludes
    excludes = excludes.map( (str) => "!"+str );

    let sources = includes.concat(excludes);

    return gulp.src(
        sources, {dot: true}
    ).pipe(
        gulp.dest(paths.target.root)
    ).pipe(
        size({title: 'copy:root'})
    );
});

gulp.task('copy:bower', () => {
    let files = bower(); // Try to get files from bower packages

    // For each bower file, add other extensions
    for (let i = files.length-1; i >= 0; i--) {
        let splitPath = files[i].split('.');
        let lastIndex = splitPath.length-1;
        if (lastIndex <= 0) continue;

        splitPath[lastIndex] = "min."+splitPath[lastIndex];
        files.push(splitPath.join('.'));

        splitPath[lastIndex] = "min.css";
        files.push(splitPath.join('.'));
    }

    files.push(path.join(paths.project.bower, '+(Materialize)/dist/**')); // Add Materialize
    files.push(path.join(paths.project.bower, '+(foundation)/**/*.+(css|js|map)')); // Add foundation

    files.push("!**/*.scss"); // Exclude SCSS, as it should be converted to CSS elsewhere

    return gulp.src(
        files
    ).pipe(
        gulp.dest(paths.source.vendor)
    ).pipe(
        gulp.dest(paths.target.vendor)
    ).pipe(
        size({title: 'copy:bower'})
    );
});
////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            SERVE                                                   //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Start hot-reload server
 */
gulp.task('serve:web', () => {
    serve({
        port: process.env.PORT || 3333,
        ui: {
            port: process.env.PORT+1 || 3334
            //  ,   weinre: { port: 8080 }
        },
        open: false,
        server: { baseDir: paths.target.root }
    });
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            WATCH                                                   //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * If any changes are made to the watched files, re-run webpack
 */
gulp.task('watch:webpack', () => {
    let watchedPaths = [
        path.join(paths.source.app.root, '**/*!(.spec.js|mock.js).js'),  // Scripts, excluding spec & mock files
        path.join(paths.source.app.root, '**/*.+(css|scss)'),    // App stylesheets
        path.join(paths.source.app.root, '**/*.html'),           // App templates
        path.join(paths.source.app.root, 'index.html')  // Index file
    ];
    gulp.watch(watchedPaths, [
        'webpack',  // Re-run the 'webpack' task
        reload      // Do a hot-reload
    ]);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                           BUILD WEBPACK                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Use webpack.config.js to build modules
 */
gulp.task('webpack', () => {
    return gulp.src(
        paths.source.app.entry
    ).pipe(
        gulpNotify({
            title: "Webpack",
            message: 'Running...',
            icon: path.join(paths.project.node, 'gulp-notify/assets/gulp.png')
        })
    ).pipe(
        webpack(require('./webpack.config'))
    ).pipe(
        gulp.dest(paths.target.root)
    ).pipe(
        gulpNotify({
            title: "Webpack",
            message: 'Finished',
            icon: path.join(paths.project.node, 'gulp-notify/assets/gulp.png')
        })
    );
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            GENERATORS                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Create a new component from the templates stored in paths.project.generators
 */
gulp.task('make:component', (done) => {
    return makeFromTemplate(paths.project.generators.component, paths.source.app.components, done);
});

/**
 * Create a new resource from the templates stored in paths.project.generators
 */
gulp.task('make:resource', (done) => {
    return makeFromTemplate(paths.project.generators.resource, paths.source.app.resources, done);
});

/**
 * Create a new directive from the templates stored in paths.project.generators
 */
gulp.task('make:directive', (done) => {
    return makeFromTemplate(paths.project.generators.directive, paths.source.app.directives, done);
});

/**
 * Create a new directive from the templates stored in paths.project.generators
 */
gulp.task('make:model', (done) => {
    return makeFromTemplate(paths.project.generators.model, paths.source.app.models, done);
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                  TESTS                                                       //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Run ESLint on javascript
 */
gulp.task('test:lint:js', (done) => {
    done();
    //TODO eslint
    //let options = require('./'+paths.project.config+'/eslint'); // Options in JS, so comments and scripting are allowed
    //options.configFile = paths.project.config+'/.eslintrc';     // Options that aren't supported in the API, only JSON config
    //
    //let target = path.join(paths.project.testResults, 'eslint');
    //mkdirp(target);
    //
    //return gulp.src([
    //    path.join(paths.source.app.root, '**/*.js'),
    //    path.join(paths.source.scripts, '**/*.js')
    //]).pipe(
    //    // eslint() attaches the lint output to the "eslint" property of the file object so it can be used by other modules.
    //    eslint(options)
    //).pipe(
    //    // Outputs the lint results to the console. Alternatively use eslint.formatEach() (see Docs).
    //    eslint.format()
    //).pipe(
    //    // Outputs the lint results to file in html format.  used by bamboo to display test result artifact.
    //    eslint.format('html', function(results) {
    //        fs.writeFileSync(path.join(target, 'report-html.html'), results);
    //    })
    //).pipe(
    //    // Outputs the lint results to file in the junit format that bamboo can parse
    //    eslint.format('junit', function(results) {
    //        fs.writeFileSync(path.join(target, 'report-junit.xml'), results);
    //    })
    //).on('error', function(error) {
    //    gutil.log('Stream Exiting With Error');
    //});
});

/**
 * Run a single Karma test
 */
gulp.task('test:karma:single', (done) => {
    // TODO protractor.config.js
    done();
    // Karma will by default exit with error status code if test fail.  Need to overide this behaviour to allow bamboo to continue processing
    // build steps.  Later test parsing tasks will identify the failed tests.
    //startKarma(true, function(exitCode) {
    //    console.log('Karma exited with exit code: ' + exitCode);
    //    console.log('Forcing exit code 0 in order allow bamboo to continue processing tasks');
    //    return process.exit(0);
    //});
});

/**
 * Start a Karma server that listens for changes
 */
gulp.task('test:karma:watch', (done) => {
    // TODO protractor.config.js
    done();
    //startKarma(false, done, 'Karma Watcher');
});

/**
 * Start a protractor end-to-end test
 */
gulp.task('test:protractor', (done) => {
    // TODO protractor.config.js
    done();
    //let specs = [
    //    path.join(paths.project.tests, "e2e/**/*.spec.js")
    //];
    //startProtractor(specs, done);
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                            MAIN TASKS                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Default task
 */
gulp.task('default', ['build']);

/**
 * Do a build
 */
gulp.task('build', (done) => {
    sync('webpack', 'copy', done);
});

/**
 * Do a clean build
 */
gulp.task('build:fresh', (done) => {
    sync('clean', 'build', done);
});

/**
 * Build, start a hot-reloading server, and watch for changes
 */
gulp.task('watch', (done) => {
    sync('build', 'serve:web', 'watch:webpack', done);
});

/**
 * Run build tests
 */
gulp.task('test', (done) => {
    sync('clean:testResults', 'test:lint:js', 'test:karma:single', done);
});
/**
 * Run end-to-end tests
 */
gulp.task('test:e2e', (done) => {
    sync('test:protractor', done);
});
/**
 * Listen for tests passed/failed upon watched file changes
 */
gulp.task('tdd', (done) => {
    sync('test:karma:watch', done);
});

/**
 * Dummy task for testing gulpfile
 */
gulp.task('dummy', () => {
    notifier.notify({
        title: "Dummy task",
        message: "Did nothing"
    });
});
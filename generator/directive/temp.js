/**
 * The '<%= kebabCaseName %>' directive
 */
let <%= camelCaseName %> = function() {
    return {
        template: 'This is the <%= kebabCaseName %> directive'
    };
};

export default <%= camelCaseName %>;

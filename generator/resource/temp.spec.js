import <%= capitalizeName %>Module   from './<%= name %>'
import <%= capitalizeName %>MockData from './<%= name %>.mockdata';

describe('<%= startCaseName %> Resource', () => {

    //////////////////////////////////////////////////////////////////////////////
    //                                  SETUP                                   //
    //////////////////////////////////////////////////////////////////////////////

    let <%= capitalizeName %>Resource, $httpBackend;

    // Load resource module
    beforeEach(window.module(<%= capitalizeName %>Module.name));

    // Fetch dependencies
    beforeEach(inject((
        _$httpBackend_
    ) => {
        $httpBackend = _$httpBackend_;
    }));

    // Inject resource
    beforeEach(inject(($injector) => {
        <%= capitalizeName %>Resource = $injector.get('<%= capitalizeName %>Resource');
    }));

    //////////////////////////////////////////////////////////////////////////////
    //                                  MODULE                                  //
    //////////////////////////////////////////////////////////////////////////////

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                 SERVICE                                  //
    //////////////////////////////////////////////////////////////////////////////

    describe('Service', () => {
        it('responds to the index request', () => {
            let url = <%= capitalizeName %>Resource.indexURL();
            let responseData = <%= capitalizeName %>MockData.index();
            let method = 'GET';

            // Mock response
            $httpBackend.when(method, url).respond(responseData);

            // Make request and assertions on response
            WorldResource.index().then(function(response){
                expect(response.status).is.equal(200);
                expect(response.config.method).is.equal(method);
                expect(response.config.url).is.equal(url);
                expect(response.config.headers.Accept).to.contain('json');
                expect(response.data).is.deep.equal(responseData);
            });

            $httpBackend.flush(); // Resolve the mocked promises
        });

        it('responds to the show request', () => {
            let ref = 1;
            let url = <%= capitalizeName %>Resource.showURL(ref);
            let responseData = <%= capitalizeName %>MockData.show(ref);
            let method = 'GET';

            // Mock response
            $httpBackend.when(method, url).respond(responseData);

            // Make request and assertions on response
            WorldResource.show(ref).then(function(response){
                expect(response.status).is.equal(200);
                expect(response.config.method).is.equal(method);
                expect(response.config.url).is.equal(url);
                expect(response.config.headers.Accept).to.contain('json');
                expect(response.data).is.deep.equal(responseData);
            });

            $httpBackend.flush(); // Resolve the mocked promises
        });

    });

    //////////////////////////////////////////////////////////////////////////////
});

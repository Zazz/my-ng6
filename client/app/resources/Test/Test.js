import angular from 'angular';
import TestResource from './Test.resource';

let TestModule = angular.module('resources.Test', []);
TestModule.service('TestResource', TestResource);

export default TestModule;
/**
 * Generates the same number, within the given range, when the same number is provided
 * @param {string} str     The source string
 * @param {int}    [min=1] Minimum value returned
 * @param {int}    [max=2] Maximum value returned
 * @returns {number}
 */
let intFromString = function(str, min=1, max=10){
    let result = 0;
    if (!str) return result;
    for (let i=0; i < str.length; i++) result += str.charCodeAt(i); // Generate a number based on the string
    return (result % (max+1-min)) + min; // Return number between min & max
};

/**
 * Functions to return mock data, given passed arguments
 */
let <%= capitalizeName %>MockData = {

    index: function() {
        return {
            list: [
                { "id": 1, slug: "num-1" },
                { "id": 2, slug: "num-2" },
                { "id": 3, slug: "num-3" },
                { "id": 4, slug: "num-4" },
                { "id": 5, slug: "num-5" },
                { "id": 6, slug: "num-6" },
                { "id": 7, slug: "num-7" },
                { "id": 8, slug: "num-8" },
                { "id": 9, slug: "num-9" },
                { "id":10, slug: "num-10"}
            ]
        };
    },

    show: function(ref) {
        let id = parseInt(ref);
        let slug = false;

        if (isNaN(id)){
            slug = ref;
            id = intFromString(ref, 1, 10);
        }else{
            slug="num-"+id;
        }

        return {
            "id":   id,
            "slug": slug
        };
    }

};

export default WorldMockData;

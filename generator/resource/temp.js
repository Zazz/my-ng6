import angular from 'angular';
import <%= capitalizeName %>Resource from './<%= name %>.resource';

/**
 * <%= slashParentPath %>/<%= capitalizeName %>Resource
 */
let <%= name %>Module = angular.module('resources<%= dotParentPath %>.<%= name %>', []);
<%= name %>Module.service('<%= capitalizeName %>Resource', <%= capitalizeName %>Resource);

export default <%= name %>Module;

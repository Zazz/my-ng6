class <%= capitalizeName %>Controller {
    constructor(
        $scope
    ) {
        this.name = "'<%= startCaseName %>' component in <%= destPath %>";
        this.$scope = $scope;
    }
}

export default <%= capitalizeName %>Controller;

import FeedModule from './Feed'
import FeedController from './Feed.controller';
import FeedComponent from './Feed.component';
import FeedTemplate from './Feed.html';

import TestResource from '../../resources/Test/Test.resource';
import TestStub from '../../resources/Test/Test.stub.js';

describe('Feed', () => {
    let makeController, $rootScope, $scope, $http, sandbox;

    //////////////////////////////////////////////////////////////////////////////
    //                                  SETUP                                   //
    //////////////////////////////////////////////////////////////////////////////

    // Load the main application module
    beforeEach(window.module(FeedModule.name));

    // Create and reset a new Sinon.js sandbox between tests
    beforeEach(function() {
        sandbox = sinon.sandbox.create();
    });
    afterEach(function() {
        sandbox.restore();
    });

    // Store dependencies
    // The injector ignores leading and trailing underscores here (i.e. _$rootScope_).
    // This allows us to inject a service but then attach it to a variable with the same name as the service.
    beforeEach(inject((
        _$rootScope_,
        _$http_
    ) => {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $http = _$http_;
    }));

    // Inject dependencies or their mocks into controller instantiated by makeController()
    beforeEach(inject(($controller) => {
        makeController = function(myTestResource=null) {

            if (!myTestResource) {
                myTestResource = new TestResource($http);
                sandbox.stub(myTestResource, 'index', TestStub.index);
                sandbox.stub(myTestResource, 'show',  TestStub.show );
            }

            // Injection locals for Controller.
            let locals = {
                $rootScope: $rootScope,
                $scope: $scope,
                TestResource: myTestResource
            };
            // Properties to add to the controller before invoking the constructor.
            // This is used to simulate the bindToController feature and simplify certain kinds of tests.
            let bindings = {

            };
            // Create an instance of the controller
            return $controller('FeedController', locals, bindings);
        };
    }));

    //////////////////////////////////////////////////////////////////////////////
    //                                  MODULE                                  //
    //////////////////////////////////////////////////////////////////////////////

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                CONTROLLER                                //
    //////////////////////////////////////////////////////////////////////////////

    describe('Controller', () => {
        // controller specs
        it('gets constructed with calls to the API', () => { // erase if removing this.name from the controller

            let resource = new TestResource($http);
            let mock = sandbox.mock(resource);

            // Fake the response for 'index'
            sandbox.stub(resource, 'index', TestStub.index);

            // Expect that 'show' is called once with the arg '3', and face the response with the same arg.
            mock.expects('show').returns(TestStub.show(3)).once().withArgs(3);

            let controller = makeController(resource);
            expect(controller).to.have.property('name');

            mock.verify();
        });
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                 TEMPLATE                                 //
    //////////////////////////////////////////////////////////////////////////////

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(FeedTemplate).to.match(/{{\s?vm\.name\s?}}/g);
        });
    });

    //////////////////////////////////////////////////////////////////////////////
    //                                COMPONENT                                 //
    //////////////////////////////////////////////////////////////////////////////

    describe('Component', () => {
        // component/directive specs
        let component = FeedComponent();

        it('includes the intended template',() => {
            expect(component.template).to.equal(FeedTemplate);
        });

        it('uses `controllerAs` syntax', () => {
            expect(component).to.have.property('controllerAs');
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(FeedController);
        });
    });

    //////////////////////////////////////////////////////////////////////////////
});

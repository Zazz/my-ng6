import karma    from 'karma';
import notifier from 'node-notifier';
import path     from 'path';
import paths    from './paths';

/**
 * Start the Karma test server
 * @param {boolean} singleRun Run once, without watchers
 * @param {function} callback The gulp callback
 * @param {string} [title='Karma Test'] The title for notifications
 * @returns {*|ProxyServer} The Karma server
 */
let startKarma = function(singleRun, callback, title='Karma Test'){
    let options = { configFile: path.join(paths.project.root, 'karma.conf.js') };
    if (typeof singleRun !== 'undefined') options.singleRun = singleRun;

    let server = new karma.Server(options, callback);

    let sendNotification = true;

    if (sendNotification) {
        server.on('browser_error', function (browser, error) {
            notifier.notify({
                title: title,
                message: 'There was an error on this browser instance',
                icon: path.join(paths.project.node, 'gulp-notify/assets/gulp-error.png')
            });
        });
        //server.on('run_start', function (browsers) { console.log('A test run starts.', browsers); });
        server.on('run_complete', function (browsers, results) {
            let notificationOptions = {title: title};
            if (results.error) {
                notificationOptions.message = "An error has occurred";
                notificationOptions.icon = path.join(paths.project.node, 'gulp-notify/assets/gulp-error.png');
            } else if (results.failed) {
                notificationOptions.message = results.failed + "/" + (results.success + results.failed) + " failed";
                notificationOptions.icon = path.join(paths.project.node, 'gulp-notify/assets/gulp-error.png');
            } else {
                notificationOptions.message = "Success";
                notificationOptions.icon = path.join(paths.project.node, 'gulp-notify/assets/gulp.png');

                /*
                 TODO: Disabled code coverage notification as it fails the first time tests are run because the files are not created in time for them to be found in this step.  Need to change the way this works.
                 let coverageLog = getCoverageSummary();
                 if (coverageLog){
                 notificationOptions.title += ": coverage";
                 notificationOptions.message = coverageLog;
                 }
                 */
            }
            if (results.disconnected) notificationOptions.title += ": Disconnected";

            notifier.notify(notificationOptions);
        });
    }
    server.start();
    return server;
};

/**
 * Get the code coverage summary
 * @returns {string|null}
 */
let getCoverageSummary = function() {
    let filename = 'coverage-summary.json';
    let filePath = path.join(paths.project.rootAbsolute, paths.project.testResults, 'coverage', filename);

    let json = require(filePath);

    if (!json) {
        let errorMessage = "ERROR: getCoverageLog("+ filename +") - Failed to find file at ["+filePath+"]";
        if (throws) throw errorMessage;
        gutil.log(errorMessage);
        return null;
    }

    let formatCoverage = function(name, obj){
        let percent = Math.round(obj.covered/obj.total * 10000) / 100;
        return name +": "+ percent +"% of "+ obj.total;
    };

    let lines      = json.total.lines;
    let statements = json.total.statements;
    let functions  = json.total.functions;
    let branches   = json.total.branches;

    let result = "";
    if (lines)      result += formatCoverage("Lines",      lines     ) +"\n";
    if (statements) result += formatCoverage("Statements", statements) +"\n";
    if (functions)  result += formatCoverage("Functions",  functions ) +"\n";
    if (branches)   result += formatCoverage("Branches",   branches  ) +"\n";

    return result;
};

export {startKarma, getCoverageSummary};
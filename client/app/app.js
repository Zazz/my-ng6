import angular from 'angular';
import Common from './common/common';
import Components from './components/components';
import Resources from './resources/resources';
import AppComponent from './app.component';

angular.module('app', [
    'ui.router',    // angular-ui-router, loaded externally
    'ngMaterial',   // angular-material, loaded externally
    'ngResource',   // angular-resource, loaded externally
    Common.name,        //
    Components.name,    //
    Resources.name      //
])

.directive('app', AppComponent);

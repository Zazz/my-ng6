exports.config = {

    // Test framework to use. This may be one of: jasmine, mocha or custom.
    framework: 'jasmine',

    // A callback function called once protractor is ready and available, and before the specs are executed.
    onPrepare: function(){
        // At this point, global variable 'protractor' object will be set up, and
        // globals from the test framework will be available. For example, if you
        // are using Jasmine, you can add a reporter with:
         //jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter('jasmine.report.xml', true, true));
    },

    // A callback function called once tests are finished. At this point, tests will be done but global objects will still be available.
    onComplete: function(data) {
      //  console.log("protractor.config.js:onComplete", data);
        //console.log("> browser", browser);
    },

    // The params object will be passed directly to the Protractor instance, and can be accessed from your test as browser.params.
    // It is an arbitrary object and can contain anything you may need in your test. This can be changed via the command line as: --params.login.user "Joe"
    params: {
    //    login: {
    //        user: 'Jane',
    //        password: '1234'
    //    }
    }
};
